# project-statistics

This project show an example of gitlab-API usage in continuous integration for getting project statistics at a schedule time.

## First do enable your project CI and the shared runners

* In `Settings - General - Visibility, project features, permission` allow `CI/CD` and `save changes`.
* In `Settings - CI/CD - Runners` set `Enable shared runners for this project` to true.

## The .gitlab-ci.yml

Only one job that does uses the gitlab API to get the [statistics](https://docs.gitlab.com/ee/api/project_statistics.html) of the project and build a csv file with the results using [jq](https://stedolan.github.io/jq/). The csv file can be downloaded in the artifacts of the job.

To make it work, you have to cerate a project access token and declare a CI/CD variable named `PROJECT_TOKEN` with the project access token as value. To do so:

1. In `Settings - Access token`, create a new project access token with 'developper' as role and check 'api' box.
2. Copy the token somewhere.
3. In `Settings - CI/CD - Variables` create a new variable with `Key` set to `PROJECT_TOKEN` and `Value` equal to the copied token.

To shedule the job you just have to go to `CI/CD - Schedule` and create a new one monthly for example on the branch you want to use (main in our example).

## Docker in docker possibility

Another version of this project using docker in docker can be found [here](https://gitlab.inria.fr/gitlabci_gallery/docker/statistics).
